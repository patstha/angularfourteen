Mon May 13 09:49:07 AM EDT 2024

# angularfourteen


This project is live at [https://angularfourteen.vercel.app](https://angularfourteen.vercel.app "fourteen!") thanks to Vercel.

```bash
System Memory
               total        used        free      shared  buff/cache   available
Mem:            15Gi       2.6Gi       1.8Gi       442Mi        11Gi        12Gi
Swap:          8.0Gi       768Ki       8.0Gi
System Storage
1018M	.
```
```bash
yarn run v1.22.22
$ ng version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/
    

Angular CLI: 17.3.7
Node: 20.13.1
Package Manager: yarn 1.22.22
OS: linux x64

Angular: 17.3.8
... animations, cdk, common, compiler, compiler-cli, core, forms
... localize, material, platform-browser
... platform-browser-dynamic, router

Package                         Version
---------------------------------------------------------
@angular-devkit/architect       0.1703.7
@angular-devkit/build-angular   17.3.7
@angular-devkit/core            17.3.7
@angular-devkit/schematics      17.3.7
@angular/cli                    17.3.7
@schematics/angular             17.3.7
rxjs                            7.5.5
typescript                      5.2.2
zone.js                         0.14.2
    
Done in 0.49s.
yarn install v1.22.22
[1/4] Resolving packages...
success Already up-to-date.
Done in 0.33s.
```
```bash
Browserslist: caniuse-lite is outdated. Please run:
  npx update-browserslist-db@latest
  Why you should do it regularly: https://github.com/browserslist/update-db#readme
Latest version:     1.0.30001617
Installed version:  1.0.30001617
caniuse-lite is up to date
caniuse-lite has been successfully updated

No target browser changes
```
```bash
yarn run v1.22.22
$ ng build --configuration production
- Generating browser application bundles (phase: setup)...
✔ Browser application bundle generation complete.
✔ Browser application bundle generation complete.
- Copying assets...
✔ Copying assets complete.
- Generating index html...
✔ Index html generation complete.

Initial chunk files           | Names         |  Raw size | Estimated transfer size
main.de15aa883d28e727.js      | main          | 247.14 kB |                64.57 kB
styles.a6281feb279fd1cf.css   | styles        |  84.60 kB |                 7.90 kB
polyfills.66d297d02ccef49b.js | polyfills     |  33.56 kB |                10.91 kB
runtime.903f7af7a5061df4.js   | runtime       | 908 bytes |               517 bytes

                              | Initial total | 366.19 kB |                83.89 kB

Build at: 2024-05-13T13:49:22.914Z - Hash: 93f2285d1f90da92 - Time: 2866ms
Done in 4.49s.
```
Mon May 13 09:49:27 AM EDT 2024

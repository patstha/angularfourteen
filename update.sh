#!/bin/bash

cd ~/src/angular/angularfourteen/;
date > README.md;
git add .;
git commit -m "add timestamp";

echo '' >> README.md 2>&1;
echo '# angularfourteen' >> README.md 2>&1;
echo '' >> README.md 2>&1;

echo '' >> README.md 2>&1;
echo 'This project is live at [https://angularfourteen.vercel.app](https://angularfourteen.vercel.app "fourteen!") thanks to Vercel.' >> README.md 2>&1;
echo '' >> README.md 2>&1;

echo "\`\`\`bash" >> README.md 2>&1;
git add .;
git commit -m "begin add system status";
echo "System Memory" >> README.md 2>&1;
free -h >> README.md 2>&1;
echo "System Storage" >> README.md 2>&1;
du -sh .>> README.md 2>&1;
git add .;
git commit -m "add system status";
echo "\`\`\`" >> README.md 2>&1;
git add .;
git commit -m "end add system status";


echo "\`\`\`bash" >> README.md 2>&1;
git add .;
git commit -m "begin update node";
[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh;  # This loads NVM
source ~/.nvm/nvm.sh;
time nvm install --lts;
time nvm use --lts;
time npm install --global @angular/cli yarn;
time yarn run ng version >> README.md 2>&1;
time yarn >> README.md 2>&1;
echo "\`\`\`" >> README.md 2>&1;
git add .;
git commit -m "end update node";

export NODE_OPTIONS="--max-old-space-size=8000";

git add .;
git commit -m "begin prepare to update angular";
time yarn run ng update @angular/core @angular/cli @angular/material @angular/localize;
git add .;
git commit -m "end prepare to update angular";

echo "\`\`\`bash" >> README.md 2>&1;
time npx update-browserslist-db@latest >> README.md 2>&1;
echo "\`\`\`" >> README.md 2>&1;

echo "\`\`\`bash" >> README.md 2>&1;
git add .;
git commit -m "begin prepare to build angular";
time yarn run ng build --configuration production >> README.md 2>&1;
echo "\`\`\`" >> README.md 2>&1;
git add .;
git commit -m "end prepare to build angular";

echo "\`\`\`bash" > locallog/fedoratest.md;
git add .;
git commit -m "begin prepare to unit test angular";
time yarn run ng test >> locallog/fedoratest.md;
echo "\`\`\`" >> locallog/fedoratest.md;
git add .;
git commit -m "end prepare to unit test angular";

date >> README.md 2>&1;
time git add .;
time git commit -m "add timestamp";
time git pull --rebase origin master --strategy-option=ours;
time git add .;
time git commit -m "merge from remote";
time git push origin master;
